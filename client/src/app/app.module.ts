import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';

import { AppComponent } from './app.component';
import { AppRouter} from './app.router';
import { HomeComponent } from './library/shared/components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GlobalHeaderComponent } from './library/shared/components/global-header/global-header.component';
import { GlobalFooterComponent } from './library/shared/components/global-footer/global-footer.component'
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { ToastrModule } from 'ngx-toastr';
import { InterceptorService } from './services/Interceptor/interceptor.service';
// import { ToastModule } from 'primeng/toast';
// import { DashboardDisplayComponent } from './library/dashboard/components/dashboard-display/dashboard-display.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

    // DashboardDisplayComponent,
    GlobalHeaderComponent,
    GlobalFooterComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRouter,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    // ToastrModule
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot()
    // ToastModule,
    
  ],
  providers: [MessageService ,
     {
    provide : HTTP_INTERCEPTORS ,
     useClass :InterceptorService ,
     multi :true
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
