import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRouter} from './user.router'

@NgModule({
  imports: [
    CommonModule,
    UserRouter
  ],
  declarations: []
})
export class UserModule { }
