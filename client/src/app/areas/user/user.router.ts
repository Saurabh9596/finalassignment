import { Routes ,RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

const routes : Routes = [
    {path : "" , loadChildren : "./modules/dishes/dishes.module#DishesModule" },
]

@NgModule({
    exports: [RouterModule],
    imports : [RouterModule.forChild(routes) ,  CommonModule]
}) 

export class UserRouter{ }
