import { Routes ,RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CreateDishComponent } from './components/create-dish/create-dish.component';
import { ViewDishComponent } from './components/view-dish/view-dish.component';

const routes : Routes = [
    {path : "create" , component : CreateDishComponent},
    {path : "dishlist" , component : ViewDishComponent},
]

@NgModule({
    exports: [RouterModule],
    imports : [RouterModule.forChild(routes) ,  CommonModule]
}) 

export class DishesRouter{ }
