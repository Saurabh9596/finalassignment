import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewDishComponent } from './components/view-dish/view-dish.component';
import { CreateDishComponent } from './components/create-dish/create-dish.component';
import {DishesRouter} from './dishes.router' 

@NgModule({
  imports: [
    CommonModule,
    DishesRouter
    // ViewDishComponent,
    // CreateDishComponent
  ],
  declarations: [ ViewDishComponent, CreateDishComponent]
})
export class DishesModule { }
