import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SericesRoutingModule } from './/serices-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SericesRoutingModule
  ],
  declarations: [FaqComponent, PrivacyPolicyComponent]
})
export class ServicesModule { }
