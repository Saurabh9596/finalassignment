import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalpagesRoutingModule } from './/globalpages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    GlobalpagesRoutingModule
  ],
  declarations: []
})
export class GlobalpagesModule { }
