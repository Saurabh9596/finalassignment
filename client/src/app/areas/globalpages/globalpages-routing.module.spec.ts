import { GlobalpagesRoutingModule } from './globalpages-routing.module';

describe('GlobalpagesRoutingModule', () => {
  let globalpagesRoutingModule: GlobalpagesRoutingModule;

  beforeEach(() => {
    globalpagesRoutingModule = new GlobalpagesRoutingModule();
  });

  it('should create an instance', () => {
    expect(globalpagesRoutingModule).toBeTruthy();
  });
});
