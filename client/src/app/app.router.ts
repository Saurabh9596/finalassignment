import { Routes ,RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './library/shared/components/home/home.component';
// import { DashboardDisplayComponent } from './library/dashboard/components/dashboard-display/dashboard-display.component';
import { AuthguardService } from './services/authguard/authguard.service';
// import { DashboardAdminComponent } from './library/dashboard/components/dashboard-admin/dashboard-admin.component';

const routes : Routes = [
    {path : "h" , loadChildren : "./areas/user/user.module#UserModule" },
    {path : "auth" , loadChildren : "./library/auth/auth.module#AuthModule"},
    {path : "globalpages" , loadChildren : "./areas/globalpages/globalpages.module#GlobalpagesModule"},
    {path : "" , component : HomeComponent },
    {path : "dashboard" , loadChildren:"./library/dashboard/dashboard.module#DashboardModule" , canActivate : [AuthguardService]},
    // {path : "dashboardAdmin" , component : DashboardAdminComponent , canActivate : [AuthguardService]},
    // {path : "manageproduct" , loadChildren:"./library/dashboard/dashboard.module#DashboardModule"}
]

@NgModule({
    exports: [RouterModule],
    imports : [RouterModule.forRoot(routes) , CommonModule]
}) 

export class AppRouter{ }
