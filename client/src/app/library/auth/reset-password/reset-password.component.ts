import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ForgotPasswordService } from '../../../services/forgotPassword/forgot-password.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: any;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private reset: ForgotPasswordService , private router : Router ,private toastr : ToastrService) { }

  ngOnInit() {
    this.resetPasswordForm = this.fb.group({
      password: new FormControl('', [Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(6)])]),
    })
  }


  FinalResetPassword() {
    this.route.params.subscribe(params => {
      this.reset.resetPassword(this.resetPasswordForm.value, params['token']).subscribe((res: any) => {
        console.log("Updated Password Response", res);
        this.toastr.success('Successsfully ',res.message,{
          timeOut: 2500
        });
        this.router.navigate(['/auth/login']);
      })
    })
  }
}
