import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ForgotPasswordService } from '../../../services/forgotPassword/forgot-password.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {


  forgotPasswordForm: any;
  constructor(private fb: FormBuilder , private forgotPassword : ForgotPasswordService ,private toastr : ToastrService) { }

  ngOnInit() {

    this.forgotPasswordForm = this.fb.group({
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    })
  }

  resetPassword(){
    this.forgotPassword.forgotPassword(this.forgotPasswordForm.value).subscribe((res:any)=>{
      console.log("Response :",res);
      this.toastr.success('Successsfully ',res.message,{
        timeOut: 2500
      });
    })
  }

}
