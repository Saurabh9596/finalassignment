import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes ,RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';



const routes : Routes = [
  {path : "login" , component : LoginComponent},
  {path : "signup" , component : SignupComponent},
  {path : "forgotPassword" , component : ForgotPasswordComponent},
  {path : "resetPassword/:token" , component : ResetPasswordComponent},
  
]

@NgModule({
  imports: [
    CommonModule , RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AuthRoutingModule { }
