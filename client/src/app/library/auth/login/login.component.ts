import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, FormControlName, Validators } from '@angular/forms';
import { LoginService } from '../../../services/userLogin/login.service';
import { MessageService } from 'primeng/api';
import { ToastService } from '../../../services/toaster/toast.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private messageService: MessageService,private toastr : ToastrService ,private toast: ToastService, private fb: FormBuilder, private userlogin: LoginService) { }
  loginForm: FormGroup;

  ngOnInit() {
    this.loginForm = new FormGroup({
      "username": new FormControl(''),
      "password": new FormControl(''),
    })
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])],
      password: ['', Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])]
    })
  }

  login() {
    this.userlogin.checklogin(this.loginForm.value).subscribe((res: any) => {
      var token = res.token;
      var user = res.data;
      localStorage.setItem("Token :", token);
      // if (token != undefined) {
        localStorage.setItem("username :", user.name);
        localStorage.setItem("role :", user.role);
        localStorage.setItem("id :", user._id);
      // }
      let name = localStorage.getItem("username :")
      if (res.code == 404 ) {
        this.toastr.error('Invalid Credentials',res.message,{
          timeOut: 2500
        });     
        this.router.navigate(['/auth/login']);  //redirected to log in page again
      }
      else if (localStorage.getItem('Token :') && (user.role == 'admin') && (user.isDeactivate == 'Active')) {
        this.toastr.success(`Welcome  ${name}`, res.message,{
          timeOut: 2500
        });
        this.router.navigate(['/dashboard/admin']);
      }
      else if (localStorage.getItem('Token :') && (user.role == 'user') && (user.isDeactivate == 'Active')) {
        this.toastr.success(`Welcome  ${name}`, res.message ,{
          timeOut: 2500
        });       
        this.router.navigate(['/dashboard/user']);
      }
    });
    console.log("Login Sucessfull", this.loginForm.value);  //this.loginForm.value
  }

}
