import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, FormControlName, Validators } from '@angular/forms';
import { AddUserDataService } from '../../../services/userSignUpDataAdd/add-user-data.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private router: Router , private toastr : ToastrService , private fb : FormBuilder , private userSignUpData : AddUserDataService ) { }
  signUpForm : FormGroup ;

  ngOnInit() {
    this.signUpForm = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "username": new FormControl(''),
      "password": new FormControl('')
    })
    this.signUpForm = this.fb.group({
      name : ['',Validators.compose([Validators.required , Validators.maxLength(30),Validators.minLength(3)])],
      email : ['',Validators.compose([Validators.required , Validators.email])],
      username : ['',Validators.compose([Validators.required , Validators.maxLength(15),Validators.minLength(3)])],
      password : ['',Validators.compose([Validators.required , Validators.maxLength(15),Validators.minLength(8)])]  // Validators.pattern('/^[^\s]+[-a-zA-Z\s]+([-a-zA-Z]+)*$')  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}[a-zA-Z_\- ]+$/$/
    })
  }

  //---function  hit the service and stores new user details within database 
  onSignUp(){
    console.log("Within Signup function");
    this.userSignUpData.addUserDet(this.signUpForm.value).subscribe((res:any)=>{
      if(res.code ==200){
      this.toastr.success('Successsfully ',res.message,{
        timeOut: 2500
      });
      this.router.navigate(['/auth/login']);
    }
    else{
      this.toastr.warning('User is Already Exist ',res.message,{
        timeOut: 2500
      });
      this.router.navigate(['/auth/signup']);
    }
      res = res.data
      console.log("Response :",res);
    })
  }
}
