import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductListService } from '../../../../services/productList/product-list.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  productlisting : any;

  constructor(private route : Router,private productlist : ProductListService) { }

  ngOnInit() {
    // this.getList()
  }
  // getList(){
  //   this.productlist.productlist().subscribe(res =>{
  //     this.productlisting = res;
  //     console.log('Product Data',this.productlisting);
  //   })
  // }
}
