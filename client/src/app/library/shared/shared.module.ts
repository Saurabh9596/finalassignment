import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { HomeComponent } from './components/home/home.component';
import { GlobalHeaderComponent } from './components/global-header/global-header.component';
import { GlobalFooterComponent } from './components/global-footer/global-footer.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  imports: [
    CommonModule,

  ],
  declarations: [GlobalHeaderComponent, GlobalFooterComponent]
})
export class SharedModule { }
