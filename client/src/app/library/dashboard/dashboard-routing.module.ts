import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
// import { DashboardDisplayComponent } from './components/dashboard-display/dashboard-display.component';
import { AddProductComponent } from './components/manage-Product/add-product/add-product.component';
// import { ProductOperationComponent } from './components/manage-Product/product-operation/product-operation.component';
import { UpdateProductDataComponent } from './components/manage-Product/update-product-data/update-product-data.component';
import { ListProductComponent } from './components/manage-Product/list-product/list-product.component';
// import { ProductOperationComponent } from './components/manage-Product/product-operation/product-operation.component';
import { ManageUsersComponent } from './components/user-management/manage-users/manage-users.component';
import { DashboardComponent } from './dashboard.component';
import { ProductOperationComponent } from './components/manage-Product/product-operation/product-operation.component';
import { EditUserComponent } from './components/user-management/edit-user/edit-user.component';
import { AddUserComponent } from './components/user-management/add-user/add-user.component';
import { AddblogComponent } from './components/blog_components/addblog/addblog.component';
import { AuthguardService } from '../../services/authguard/authguard.service';
import { AdminguardService } from '../../services/AdminGuard/adminguard.service';
import { UserguardService } from '../../services/UserGuard/userguard.service';
import { ListBlogComponent } from './components/blog_components/list-blog/list-blog.component';
import { UserHomeComponent } from './components/user/user-home/user-home.component';
import { UserProductDisplayComponent } from './components/user/user-product-display/user-product-display.component';
import { UserListblogComponent } from './components/user/user-listblog/user-listblog.component';
import { UserProductCartComponent } from './components/user/user-product-cart/user-product-cart.component';
import { EditprofileComponent } from './components/editprofile/editprofile.component';
import { EditBlogComponent } from './components/blog_components/edit-blog/edit-blog.component';
import { OrderSummaryComponent } from './components/user/order-summary/order-summary.component';
import { EditProfileAdminComponent } from './components/edit-profile-admin/edit-profile-admin.component';
// import { UserHomeComponent } from './components/User/user-home/user-home.component';

const routes: Routes = [
  {
    path: "admin", component: DashboardComponent, canActivate: [AdminguardService],
    children: [
      //Admin Routing
      { path: "manageUser", component: ManageUsersComponent },
      { path: "editUser/:id", component: EditUserComponent },
      { path: "addUser", component: AddUserComponent },
      { path: "editprofile/:id", component: EditProfileAdminComponent },

      //Product Routing
      { path: "manageproduct", component: ProductOperationComponent },
      { path: "addproduct", component: AddProductComponent },
      { path: "listProduct", component: ListProductComponent },
      { path: "updateProduct/:id", component: UpdateProductDataComponent },
      //Blogs routing
      { path: "addblog", component: AddblogComponent },
      { path: "listblog", component: ListBlogComponent },
      { path: "editblog/:id", component: EditBlogComponent },

    ]
  },
  {
    path: "user", component: UserHomeComponent, canActivate: [UserguardService], children: [
      { path: "", component: UserProductDisplayComponent },
      { path: "listblog", component: UserListblogComponent },
      { path: "listcart", component: UserProductCartComponent },
      { path: "ordersummary", component: OrderSummaryComponent },
      { path: "editprofile/:id", component: EditprofileComponent },


    ]
  }



]
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class DashboardRoutingModule { }
