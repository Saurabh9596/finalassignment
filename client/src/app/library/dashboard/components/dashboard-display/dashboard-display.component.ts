import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard-display',
  templateUrl: './dashboard-display.component.html',
  styleUrls: ['./dashboard-display.component.scss']
})
export class DashboardDisplayComponent implements OnInit {

 userId : any ;
  constructor(private route : Router ,private toastr : ToastrService) { }
  name : String
  ngOnInit() {
    this.name = localStorage.getItem('username :'); 
    this.userId = localStorage.getItem('id :');
  }

  onLogout(){
    localStorage.clear();
    this.toastr.success('Successsfully ',"Log Out",{
      timeOut: 2500
    });
    console.log('localstorage clear');
    this.route.navigate(['/auth/login']);
  }
}
