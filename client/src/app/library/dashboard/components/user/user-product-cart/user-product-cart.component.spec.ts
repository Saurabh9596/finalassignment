import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProductCartComponent } from './user-product-cart.component';

describe('UserProductCartComponent', () => {
  let component: UserProductCartComponent;
  let fixture: ComponentFixture<UserProductCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProductCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProductCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
