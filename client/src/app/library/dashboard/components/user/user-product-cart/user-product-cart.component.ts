import { Component, OnInit, ElementRef, ViewChild, Input, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ListCartService } from '../../../../../services/listCart-Product/list-cart.service';
// import { RemoveItemFromCartService } from '../../../../../services/removeItemFromCart/remove-item-from-cart.service';
import { EditCartService } from '../../../../../services/editCart/edit-cart.service';
import { AddToOrdersService } from '../../../../../services/addToOrders/add-to-orders.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { PaymentService } from '../../../../../services/paymentService /payment.service';
import { RemoveItemFromCartService } from '../../../../../services/deleteItemFromCart/remove-item-from-cart.service';
import { DeleteCartService } from '../../../../../services/deleteCart/delete-cart.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-product-cart',
  templateUrl: './user-product-cart.component.html',
  styleUrls: ['./user-product-cart.component.scss']
})
export class UserProductCartComponent implements OnInit , OnDestroy ,AfterViewInit {
  cartData: any[] = [];
  productQuantity: number;
  display: boolean = false;
  displayAddress : boolean = false;
  displayButton : boolean = false;
  id: any;
  totalbill: number = 0;
  cities1: SelectItem[];
  addressForm : FormGroup ;
  orderId : any;
  data : any ;
  disabled : boolean = true;
  // products: any;
  address : string;
  displayPayment : boolean = false ;

  @Input() amount: number;
  @Input() label: string;
  elements: any;
  paymentRequest: any;
  prButton: any;
  emailAddress: any;

  @ViewChild('payElement') payElement;

  @ViewChild('cardInfo') cardInfo: ElementRef;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  
  constructor(private ListCart: ListCartService, private toastr : ToastrService,private cd : ChangeDetectorRef, private payment : PaymentService,private DeleteFromCart: RemoveItemFromCartService ,private deleteCart : DeleteCartService , private EditCart : EditCartService , private addOrder : AddToOrdersService , private fb : FormBuilder) { 
    this.cities1 = [
      {label:'Select City', value:null},
      {label:'Nagpur', value: 'nagpur'},
      {label:'Pune', value:'pune'},
      {label:'Mumbai', value: 'mumbai'},  
  ]; 
  }

  ngOnInit() {
    this.getCartList();

    this.addressForm = this.fb.group({
      address : new FormControl('', [Validators.compose([Validators.required, Validators.maxLength(80)])]),
      pincode : new FormControl('', [Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])]),
      city : new FormControl('', [Validators.compose([Validators.required])]),
      state : new FormControl('Maharashtra'),
      country :new FormControl('India')
    })
  }

  getCartList() {
     this.id = localStorage.getItem('id :');
    this.ListCart.cartlist(this.id).subscribe(res => {
      console.log('Cart List', res);
      if(res.length ==0){
        this.displayButton = true;
      }
      this.cartData = res;
      this.productQuantity = res.quantity;
      this.totalbill = 0;
      for (let i = 0; i < res.length; i++) {
        this.totalbill = this.totalbill + res[i].totalbill;
      }
      console.log('Total Bill ', this.totalbill);
      console.log('Cart Data', this.cartData);
    })
  }

  updateCart(pId, pCost, pQty) {
    console.log("yeahhhhhhhhh", pId, pQty)
    let data = {
      userId: localStorage.getItem("id :"),
      productId: pId,
      productCost: pCost,
      quantity: pQty,
    }
    console.log("***********", data)
    this.EditCart.editCart(data)
      .subscribe((res: any) => {
        // res = res;
        console.log("Response after change qty",res);
        this.getCartList();
      });
  }

  proceedToCheckout(){
    this.address = "Address - "+ this.addressForm.value.address + " , "+ this.addressForm.value.city + " , "+this.addressForm.value.pincode + " , "+this.addressForm.value.state+ " , " + this.addressForm.value.country
    console.log(this.cartData,"sadsadsa",this.totalbill)
    console.log("Address ",this.address);
    console.log(this.id)
    this.addressForm.reset();
    this.displayAddress = false;
    this.displayPayment = true;
    let data= {
      total: this.totalbill,
      userId: this.id,
      address: this.address,
      paymentMethod: "Card Payment",  
    }
    this.addOrder.addToOrders(data)
      .subscribe((res: any) => {
        console.log("After Checkout Done , Response :",res);
        this.orderId = res.data._id;
        this.getCartList();
      });
    

}

  removeFromCart(id) {
    console.log("Product Id to remove from cart", id);
    this.DeleteFromCart.deleteItem(id).subscribe(res => {
      console.log('Updated Cart Data after delete', res);
      this.toastr.success('Successsfully ',res.message,{
        timeOut: 2500
      });
      this.display = false;
      this.getCartList();
    })
  }
  showDialog(id) {
    this.display = true;
    this.id = id;
    console.log(this.id);
  }

  showDialogAddressModal(){
    this.displayAddress = true;
  }

 
  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'purple'
        }
      }
    };
    this.card = elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }
  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card, {
      email: this.emailAddress
    });

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.data = {
        amount: this.totalbill,
        stripeToken: token.id,
        userId: this.id
      }
      console.log('Success!', this.data);
      this.payment.makePayment(this.data)
        .subscribe((res: any) => {
         console.log("Payment Successfully Done !",res);
          this.getCartList();
          this.toastr.success('Successsfully ',res.message,{
            timeOut: 2500
          });
        });
        localStorage.setItem("OrderId :",this.orderId);
      
        this.deleteCart.deleteCart(this.id).subscribe(res => {
          console.log("Response after cart flush",res);
         })
         this.displayPayment = false;
      // ...send the token to the your backend to process the charge
    }
  }
}




