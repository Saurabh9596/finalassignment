import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { MessageService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {
  productlisting : any;
  userId : any ;
  userName : any ;
  constructor(private route : Router, private toastr : ToastrService,private productlist : ProductListService ,private messageService : MessageService) { }

  ngOnInit() {

    this.messageService.add({severity:'success', summary: 'Success !', detail:'Login Successfully!'});
    this.getList();
    this.userId = localStorage.getItem('id :');
    this.userName = localStorage.getItem('username :');
    console.log("Username ",this.userName)
  }
  getList(){
    this.productlist.productlist().subscribe(res =>{
      this.productlisting = res;
      console.log('Product Data',this.productlisting);
    })
  }
  onLogout(){
    localStorage.clear();
    this.toastr.success('Successsfull ',"Log Out",{
      timeOut: 2500
    });
    console.log('localstorage clear');
    this.route.navigate(['/auth/login']);
  }
}
