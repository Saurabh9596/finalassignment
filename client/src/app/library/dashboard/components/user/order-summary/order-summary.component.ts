import { Component, OnInit } from '@angular/core';
import { OrderSummaryService } from '../../../../../services/getOrderSummary/order-summary.service';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {

  userId: any;
  orderRes: any;
  totalbill: number = 0;
  orderFirstRecord : any;

  // orderData: any;
  constructor(private getOrderSummary: OrderSummaryService) { }

  ngOnInit() {
    this.getOrdersSummary();
  }


  getOrdersSummary() {
    this.userId = localStorage.getItem('id :');
    this.getOrderSummary.getOrderSummary(this.userId).subscribe(res => {
      console.log('Response of Order Summary', res);
      // this.totalbill = 0;
      // this.orderFirstRecord = res[0];
      // console.log('first record', this.orderFirstRecord);
      // for (let i = 0; i < this.orderFirstRecord.cartData.length; i++) {
      //   this.totalbill = this.totalbill + this.orderFirstRecord[i].totalbill;
      // }
      this.orderRes = res;
     
    })

  }
}
