import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListblogComponent } from './user-listblog.component';

describe('UserListblogComponent', () => {
  let component: UserListblogComponent;
  let fixture: ComponentFixture<UserListblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
