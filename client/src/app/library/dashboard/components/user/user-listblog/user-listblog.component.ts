import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddcommentService } from '../../../../../services/addComment/addcomment.service';
import { Router } from '@angular/router';
import { ListblogService } from '../../../../../services/listblog/listblog.service';
import { ListCommentService } from '../../../../../services/listcomment/list-comment.service';

@Component({
  selector: 'app-user-listblog',
  templateUrl: './user-listblog.component.html',
  styleUrls: ['./user-listblog.component.scss']
})
export class UserListblogComponent implements OnInit {
  val1: number;
  addCommentForm: FormGroup;
  bloglisting: any;
  commentlisting: any
  name : string
  constructor(private fb: FormBuilder, private AddComment: AddcommentService, private blogList: ListblogService, private router: Router, private listComent: ListCommentService) { }

  ngOnInit() {
    this.addCommentForm = this.fb.group({
      comment: new FormControl(''),
      rating: new FormControl(''),
    })
    this.getList();

    // this.getCommentList();
  }

  getList() {
    this.blogList.bloglist().subscribe(res => {
      this.bloglisting = res;
      console.log('Product Data', this.bloglisting);
    })
  }
  private saveBlogValue(blogId): any {
    let id = localStorage.getItem('id :');
  this.name = localStorage.getItem('username :');
    console.log("Token : ", id);
    let input = new FormData();
    let ipRating: number = parseInt(this.addCommentForm.get('rating').value) | 0;
    let rating = ipRating.toString();
    console.log('Rating', rating);
    input.append('comment', this.addCommentForm.get('comment').value);
    input.append('userId', id);
    input.append('blogId', blogId);
    input.append('rating', rating);
    input.append('userName', this.name);
    console.log("Entered Comment-", input);
    return input;
  }

  getCommentList(res,blogId){
    this.listComent.commentlist(res,blogId).subscribe(res =>{
      console.log("Response of Comments of particular blog:",res);
      this.commentlisting = res;
      console.log('Comment list',this.commentlisting);
    })
  }

  addComment(id) {
    const formModel = this.saveBlogValue(id);
    console.log("FormModel:", formModel);
    this.AddComment.addComment(formModel).subscribe((res: any) => {
      console.log("Response :", res);
      res.userName = name
      this.addCommentForm.reset();
      location.reload();
      // this.getCommentList(res,id);
    })
  }
}
