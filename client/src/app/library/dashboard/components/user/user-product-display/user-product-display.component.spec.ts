import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProductDisplayComponent } from './user-product-display.component';

describe('UserProductDisplayComponent', () => {
  let component: UserProductDisplayComponent;
  let fixture: ComponentFixture<UserProductDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProductDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProductDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
