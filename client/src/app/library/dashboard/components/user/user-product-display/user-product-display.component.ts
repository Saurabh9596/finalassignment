import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { AddToCartService } from '../../../../../services/addToCart/add-to-cart.service';
import { MessageService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-product-display',
  templateUrl: './user-product-display.component.html',
  styleUrls: ['./user-product-display.component.scss']
})
export class UserProductDisplayComponent implements OnInit {
  productlisting: any;
  cartlisting: any;
  productId: string;
  products = [
    { pname: "IphoneX", discount: "10% Discount", img: "../../../../../../assets/electrovilla_images/iphoneX.jpg" },
    { pname: "Mi Band 3", discount: "at EMI just of Rs.499", img: "../../../../../../assets/electrovilla_images/miBand3.jpg" },
    { pname: "Google Glass ", discount: "Introduces to the Augumented World", img: "../../../../../../assets/electrovilla_images/gg2.jpg" },
    { pname: "SmartWatch", discount: "Additional Discount with Axis ", img: "../../../../../../assets/electrovilla_images/smartWatch.jpeg" },
    { pname: "Smart Tv", discount: "Additional 5% Discount with HDFC ", img: "../../../../../../assets/electrovilla_images/smartTv.png" },
    { pname: "Smart Bot", discount: "Future is Here ", img: "../../../../../../assets/electrovilla_images/l1.png" },

  ];
  constructor( private productlist: ProductListService,private toastr : ToastrService, private Addtocart: AddToCartService) { }

  ngOnInit() {
    this.getList()
  }

  getList() {
    this.productlist.productlist().subscribe(res => {
      this.productlisting = res;
      console.log('Product Data is here', this.productlisting);
    })
  }

  getProdId(id) {
    this.productId = id;
  }
  addToCart(id, productCost) {
    let quantity: number = 1;
    let userId = localStorage.getItem('id :');
    console.log("Id of Product", id);
    this.getProdId(id);
    console.log("Quantity ", quantity);
    let data = {
      productId: id,
      quantity: quantity,
      userId: userId,
      productCost: productCost
    }
    this.Addtocart.addtocart(data).subscribe(res => {
      this.cartlisting = res;
      this.toastr.success('Success ',"Product Added to Cart",{
        timeOut: 2500
      });
      console.log('Cart Data', this.cartlisting);
    })
  }

}
