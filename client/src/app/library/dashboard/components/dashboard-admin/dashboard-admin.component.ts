import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit {

  constructor(private route : Router ,private toastr : ToastrService) { }

  ngOnInit() {
  }
  onLogout(){
    localStorage.clear();
    this.toastr.success('Successsfully ',"Log Out",{
      timeOut: 2500
    });
    console.log('localstorage clear');
    this.route.navigate(['/auth/login']);
  }
}
