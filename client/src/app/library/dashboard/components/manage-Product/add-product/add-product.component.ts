import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AddProductService } from '../../../../../services/addProduct/add-product.service';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  constructor(private router: Router, private fb: FormBuilder,private toastr : ToastrService ,private addProdDet: AddProductService,private productlist : ProductListService) { }

  addProductForm: FormGroup;
  imgurl:any;
  productlisting : any;
  ngOnInit() {
   
    this.addProductForm = this.fb.group({
      productName: new FormControl('', [Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])]),
      productCost: new FormControl('', [Validators.compose([Validators.required])]),
      description: new FormControl('', [Validators.compose([Validators.required])]),
      shippingCost: new FormControl('', [Validators.compose([Validators.minLength(2)])]),
      delStatus: new FormControl('', [Validators.compose([Validators.required])]),
      imgFile: null
    })
  }

  upload(event) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event : ProgressEvent)=>{
        this.imgurl = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("After uploading file", file);
      this.addProductForm.get('imgFile').setValue(file);
    }
  }
  private saveProductValue(): any {
    let input = new FormData();
    input.append('productName', this.addProductForm.get('productName').value);
    input.append('productCost', this.addProductForm.get('productCost').value);
    input.append('description', this.addProductForm.get('description').value);
    input.append('shippingCost', this.addProductForm.get('shippingCost').value);
    input.append('delStatus', this.addProductForm.get('delStatus').value);
    input.append('imageUrl', this.addProductForm.get('imgFile').value);
    console.log("Product Data after appending ", input);
    return input;
  }
  addProduct(){
    const formModel = this.saveProductValue();
    this.addProdDet.addProductDet(formModel).subscribe((res: any) => {
      this.toastr.success('Successsfully ',res.message,{
        timeOut: 2500
      });
      console.log("Response :", res);
    })
    this.productlist.productlist().subscribe(res =>{
      this.productlisting = res;
      console.log('Product Data',this.productlisting);
      this.router.navigate(['/dashboard/admin/listProduct']);
    })
  }
 
}
