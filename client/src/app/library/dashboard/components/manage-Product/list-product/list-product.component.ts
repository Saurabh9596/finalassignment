import { Component, OnInit } from '@angular/core';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { DeleteProductService } from '../../../../../services/deleteProduct/delete-product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {
  productlisting : any;
  display: boolean = false;
  id : any ;

  constructor(private productlist : ProductListService, private toastr : ToastrService,private deleteProduct : DeleteProductService) { }

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.productlist.productlist().subscribe(res =>{
      this.productlisting = res;
      console.log('Product Data',this.productlisting);
    })
  }

  deleteItem(id){
    this.deleteProduct.deleteItem(id).subscribe(res=>{
      console.log("Data Deleted");
      this.toastr.success('Successsfully ',res.message,{
        timeOut: 2500
      });
      this.display = false;
      this.getList();
    })
  }
  
  showDialog(id) {
    this.display = true;
    this.id= id;
    console.log(this.id);
  }
}
