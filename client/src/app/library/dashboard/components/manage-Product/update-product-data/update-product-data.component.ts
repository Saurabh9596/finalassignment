import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UpdateProductService } from '../../../../../services/updateProductData/update-product.service';
import { GetProductDetailsService } from '../../../../../services/getProductDetails/get-product-details.service';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-product-data',
  templateUrl: './update-product-data.component.html',
  styleUrls: ['./update-product-data.component.scss']
})
export class UpdateProductDataComponent implements OnInit {

  updateDataForm: FormGroup
  root: Router
  productData: any;
  imgurl : any;
  productlisting : any;
  constructor(private editData: UpdateProductService,private toastr : ToastrService ,private getProdData: GetProductDetailsService,private productlist : ProductListService, private route: ActivatedRoute, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
  
    this.updateDataForm = this.fb.group({
      productName: new FormControl('', [Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])]),
      productCost: new FormControl('', [Validators.compose([Validators.required])]),
      description: new FormControl('', [Validators.compose([Validators.required])]),
      shippingCost: new FormControl('', [Validators.compose([Validators.minLength(2)])]),
      delStatus: new FormControl('', [Validators.compose([Validators.required])]),
      imgFile: null
    })
    this.route.params.subscribe(params => {
      this.getProdData.getProductData(params['id']).subscribe((res: any) => {
        console.log("Result : ", res);
        this.productData = res;
        console.log("after getting data from getproductDetails",this.productData);
        this.updateDataForm.patchValue({
          productName : this.productData.productName,
          productCost : this.productData.productCost,
          description : this.productData.description,
          shippingCost : this.productData.shippingCost,
          delStatus : this.productData.delStatus,
          imgurl : this.productData.imageUrl  
        })
      })
    })
  }
  upload(event) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event : ProgressEvent)=>{
        this.imgurl = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("After uploading file", file);
      this.updateDataForm.get('imgFile').setValue(file);
    }
  }
  private saveProductValue(): any {
    let input = new FormData();
    input.append('productName', this.updateDataForm.get('productName').value);
    input.append('productCost', this.updateDataForm.get('productCost').value);
    input.append('description', this.updateDataForm.get('description').value);
    input.append('shippingCost', this.updateDataForm.get('shippingCost').value);
    input.append('delStatus', this.updateDataForm.get('delStatus').value);
    input.append('imageUrl', this.updateDataForm.get('imgFile').value);
    console.log("Product Data after appending ", input);
    return input;
  }

  update(){
    const formModel = this.saveProductValue();
    this.route.params.subscribe(params => {
      this.editData.updateData(formModel, params['id']).subscribe((res: any) => {
        console.log("Updated product data Response",res);
        this.toastr.success('Successsfully ',res.message,{
          timeOut: 2500
        });
      })
      this.productlist.productlist().subscribe(res =>{
        this.productlisting = res;
        console.log('Product Data',this.productlisting);
        this.router.navigate(['/dashboard/admin/listProduct']);
      })
    })
  }
}   
