import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdduserService } from '../../../../../services/adduser/adduser.service';
import { ProductListService } from '../../../../../services/productList/product-list.service';
import { ManageUsersComponent } from '../manage-users/manage-users.component';
import { ListusersService } from '../../../../../services/listUsers/listusers.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  constructor(private router: Router, private fb: FormBuilder, private toastr: ToastrService, private adduser: AdduserService, private userlist: ListusersService) { }
  addUserForm: FormGroup;
  userslisting: any;
  ngOnInit() {
    this.addUserForm = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "role": new FormControl(''),
      "username": new FormControl(''),
      "password": new FormControl('')
    })

    this.addUserForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.minLength(3)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      role: ['', Validators.compose([Validators.required])],
      username: ['', Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])],
      password: ['', Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(8)])]
    })
  }
  addUser() {
    this.adduser.addUser(this.addUserForm.value).subscribe((res: any) => {
      console.log("Response :", res);
      this.toastr.success('Successsfully ', res.message, {
        timeOut: 2500
      });
      
      this.userlist.userslist().subscribe(res => {
        this.userslisting = res;
        console.log('User List', this.userslisting);
        this.router.navigate(['/dashboard/admin/manageUser']);
      })
    })

    // this.router.navigate(['/dashboard/manageUser']);
  }
}
