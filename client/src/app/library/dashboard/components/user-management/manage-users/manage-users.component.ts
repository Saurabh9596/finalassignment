import { Component, OnInit } from '@angular/core';
import { ListusersService } from '../../../../../services/listUsers/listusers.service';
import { DeleteuserService } from '../../../../../services/deleteuser/deleteuser.service';
import { ActivatedRoute } from '@angular/router';
import { DeactivateService } from '../../../../../services/deactivateUser/deactivate.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

  userslisting : any;
  display: boolean = false;
  id : any ;

  constructor(private userslist : ListusersService ,  private toastr: ToastrService, private deleteuser : DeleteuserService,private route : ActivatedRoute , private deactivateuser : DeactivateService) { }

  ngOnInit() {
    this.getUserList();
  }

  getUserList(){
    this.userslist.userslist().subscribe(res=>{
      this.userslisting = res;
      console.log("Users List",this.userslisting);
    })
  }

  deleteUser(id){
    this.deleteuser.deleteUser(id).subscribe(res=>{
      console.log("User Data Deleted");
      this.display = false;
      this.toastr.success('Successsfully ', res.message, {
        timeOut: 2500
      });
      this.getUserList();
    })

  }

  showDialog(id) {
    this.display = true;
    this.id= id;
    console.log(this.id);
  }
  
  deactivate(id){
   
      this.deactivateuser.deactivateUser(id).subscribe((res:any) =>{
        console.log('Res',res);
        this.toastr.warning('', res.message, {
          timeOut: 2500
        });
        this.getUserList();
      })
  }
}



// //        res = res.isDeactivate;
// console.log('Status1',res);
// if(res =="Active"){
//   this.status ="Active"; 
// }
// else{
//   this.status ="Deactive"; 
// }
// console.log('Status2',this.status);