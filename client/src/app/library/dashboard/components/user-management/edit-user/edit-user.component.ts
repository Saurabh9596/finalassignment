import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EdituserService } from '../../../../../services/editUser/edituser.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ListusersService } from '../../../../../services/listUsers/listusers.service';
import { GetUserDataService } from '../../../../../services/getUserData/get-user-data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  editUserForm : FormGroup;
  userslisting : any
  userData : any;

  constructor(private fb : FormBuilder , private toastr: ToastrService, private edituser : EdituserService , private router : Router,private route : ActivatedRoute,private userslist : ListusersService ,private getUserData : GetUserDataService ) { }

  ngOnInit() {
    this.editUserForm = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "role": new FormControl(''),
      "password": new FormControl('')
    })
    this.editUserForm = this.fb.group({
      name : ['',Validators.compose([Validators.required , Validators.maxLength(30),Validators.minLength(3)])],
      email : ['',Validators.compose([Validators.required , Validators.email])],
      role : ['',Validators.compose([Validators.required])],
      password : ['',Validators.compose([Validators.required , Validators.maxLength(15),Validators.minLength(8)])]
    })

    this.route.params.subscribe(params => {
      this.getUserData.getUserData(params['id']).subscribe((res: any) => {
        console.log("Result : ", res);
        this.userData = res;
        console.log("after getting data from getUserData",this.userData);
        this.editUserForm.patchValue({
          name : this.userData.name,
          email : this.userData.email,
          role : this.userData.role,
          password : this.userData.password,
        })
      })
    })
  }
  getUserList(){
    this.userslist.userslist().subscribe(res=>{
      this.userslisting = res;
      console.log("Users List",this.userslisting);
      this.router.navigate(['/dashboard/admin/manageUser'])
    })
  }
  onEdit(){
    this.route.params.subscribe(params => {
      this.edituser.editUser(this.editUserForm.value, params['id']).subscribe((res:any) =>{
        this.toastr.success('Successsfully ', res.message, {
          timeOut: 2500
        });
        // res = res.data;
        this.getUserList();
      })
    })
  }

}
