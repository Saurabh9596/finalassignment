import { Component, OnInit } from '@angular/core';
import { ListblogService } from '../../../../../services/listblog/listblog.service';
import { DeleteblogService } from '../../../../../services/deleteblog/deleteblog.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { GetBlogCommentService } from '../../../../../services/getBlogComment/get-blog-comment.service';
import { AddcommentService } from '../../../../../services/addComment/addcomment.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.scss']
})
export class ListBlogComponent implements OnInit {
  bloglisting : any;
  addCommentForm: FormGroup;
  BlogId: any;
  comments: any;
  name : string;
  data: any;
  id : any;
  displayDialog : boolean = false;
  display: boolean = false;

  constructor(private bloglist : ListblogService ,  private toastr: ToastrService ,private DeleteBlog : DeleteblogService ,private getBlogComment : GetBlogCommentService, private addComment : AddcommentService , private getComment : GetBlogCommentService , private fb : FormBuilder) { }

  ngOnInit() {
    this.addCommentForm = this.fb.group({
      comment: new FormControl(''),

    })
    this.getList()
  }
  getList(){
    this.bloglist.bloglist().subscribe(res =>{
      this.bloglisting = res;
      console.log('Product Data',this.bloglisting);
    })
  }

  private saveBlogValue(BlogId): any {
  this.name = localStorage.getItem('username :');
  this.id = localStorage.getItem('id :');

    let input = new FormData();
    let ipRating: number =  0;  //parseInt(this.addCommentForm.get('rating').value) |
    let rating = ipRating.toString();
    input.append('comment', this.addCommentForm.get('comment').value);
    input.append('blogId',BlogId);
    input.append('rating', rating);
    input.append('userId', this.id);
    input.append('userName', this.name);
    console.log("Entered Comment- **********************", input);
    return input;
  }

  deleteItem(id){
    this.DeleteBlog.deleteBlog(id).subscribe(res=>{
      console.log("Data Deleted");
      this.displayDialog = false;
      this.toastr.success('Successsfully ', res.message, {
        timeOut: 2500
      });
      this.getList();
    })
  }

  showDeleteDialog(id) {
    this.displayDialog = true;
    this.id= id;
    console.log(this.id);
  }

  showDialog(id) {
    this.getBlogComment.getComments(id).subscribe(res => {
      this.comments = res;
      this.BlogId = id
      console.log("Blog id inside showDialog",this.BlogId);
    });
    this.display = true;
  }

  onSubmit(id) {
    // if (!this.addCommentForm.value.comment) { return null }
    console.log("***********", id);
    const formModel = this.saveBlogValue(id);
    console.log("FormModel:", formModel);
    this.addComment.addComment(formModel)
      .subscribe((res: any) => {
        console.log('Comment Add hone ke baad ka response',res);
        res = res.data;
      });
    this.addCommentForm.reset();
    this.display = false;
  }

}
