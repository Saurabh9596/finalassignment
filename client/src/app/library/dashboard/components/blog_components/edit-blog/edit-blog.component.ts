import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EditblogService } from '../../../../../services/editblog/editblog.service';
import { GetProductDetailsService } from '../../../../../services/getProductDetails/get-product-details.service';
import { ListblogService } from '../../../../../services/listblog/listblog.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.scss']
})
export class EditBlogComponent implements OnInit {

  editBlogForm: any;
  blogData: any;
  imgurl: any;
  blogListing: any;
  constructor(private editBlog: EditblogService,  private toastr: ToastrService,private bloglist: ListblogService, private route: ActivatedRoute, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {

    this.editBlogForm = this.fb.group({
      title: new FormControl('', [Validators.compose([Validators.required, Validators.maxLength(15), Validators.minLength(3)])]),
      description: new FormControl('', [Validators.compose([Validators.required])]),
      imgFile: null
    })
    this.route.params.subscribe(params => {
      this.editBlog.getBlogData(params['id']).subscribe((res: any) => {
        this.blogData = res;
        console.log("after getting data from getBlogDetails", this.blogData);
        this.editBlogForm.patchValue({
          title: this.blogData.title,
          description: this.blogData.description,
          imgurl: this.blogData.imageUrl
        })
      })
    })

  }

  upload(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: ProgressEvent) => {
        this.imgurl = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("After uploading file", file);
      this.editBlogForm.get('imgFile').setValue(file);
    }
  }
  private saveProductValue(): any {
    let input = new FormData();
    input.append('title', this.editBlogForm.get('title').value);
    input.append('description', this.editBlogForm.get('description').value);
    input.append('imageUrl', this.editBlogForm.get('imgFile').value);
    console.log("Product Data after appending ", input);
    return input;
  }

  update() {
    const formModel = this.saveProductValue();
    this.route.params.subscribe(params => {
      this.editBlog.EditBlog(formModel, params['id']).subscribe((res: any) => {
        // res = res;
        console.log("Updated product data Response", res);
        this.toastr.success('Successsfully ', res.message, {
          timeOut: 2500
        });
      })
      this.bloglist.bloglist().subscribe(res => {
        this.blogListing = res;
        console.log('Product Data', this.blogListing);
        this.router.navigate(['/dashboard/admin/listblog']);
      })
    })
  }

}
