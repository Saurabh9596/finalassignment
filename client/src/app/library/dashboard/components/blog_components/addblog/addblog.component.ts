import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AddblogService } from '../../../../../services/addBlog/addblog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.scss']
})
export class AddblogComponent implements OnInit {
  addBlogForm : FormGroup;
  imgurl : any
  constructor(  private fb: FormBuilder, private router: Router ,  private toastr: ToastrService,private addblog : AddblogService) { }

  ngOnInit() {
    this.addBlogForm = this.fb.group({
      blogtitle: new FormControl('', [Validators.compose([Validators.required, Validators.minLength(3)])]),
      date : new FormControl('', [Validators.compose([Validators.required])]),
      description: new FormControl('', [Validators.compose([Validators.required])]),
      imgFile: null
  })
}
upload(event) {
  if(event.target.files && event.target.files[0]) {
    var reader = new FileReader();
    reader.onload = (event : ProgressEvent)=>{
      this.imgurl = (<FileReader>event.target).result;
    }
    reader.readAsDataURL(event.target.files[0]);
    let file = event.target.files[0];
    console.log("After uploading file", file);
    this.addBlogForm.get('imgFile').setValue(file);
  }
}
private saveBlogValue(): any {
  let input = new FormData();
  input.append('title', this.addBlogForm.get('blogtitle').value);
  input.append('description', this.addBlogForm.get('description').value);
  input.append('date', this.addBlogForm.get('date').value);
  input.append('imageUrl', this.addBlogForm.get('imgFile').value);
  console.log("Product Data after appending ", input);
  return input;
}
addBlog(){
  const formModel = this.saveBlogValue();
    this.addblog.addBlog(formModel).subscribe((res: any) => {
      this.toastr.success('Successsfully ', res.message, {
        timeOut: 2500
      });
      console.log("Response :", res);
      this.router.navigate(['/dashboard/admin/listblog']);
    })

}
}
