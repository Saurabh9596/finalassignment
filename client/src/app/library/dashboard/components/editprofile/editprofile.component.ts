import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EditprofileService } from '../../../../services/editProfile/editprofile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GetUserDataService } from '../../../../services/getUserData/get-user-data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {
  
  editUserForm : FormGroup;
  userslisting : any
  userData : any;

  constructor(private fb : FormBuilder ,private toastr : ToastrService , private editProfile : EditprofileService , private router : Router,private route : ActivatedRoute,private getUserData : GetUserDataService ) { }

  ngOnInit() {
    this.editUserForm = new FormGroup({
      "fullname": new FormControl(''),
      "username": new FormControl(''),
      "email": new FormControl(''),
      "role": new FormControl(''),
      "password": new FormControl('')
    })
    this.editUserForm = this.fb.group({
      fullname : ['',Validators.compose([Validators.required , Validators.maxLength(25),Validators.minLength(3)])],
      username : ['',Validators.compose([Validators.required , Validators.maxLength(25),Validators.minLength(3)])],
      email : ['',Validators.compose([Validators.required , Validators.email])],
      role : ['user',Validators.compose([Validators.required])],
      password : ['',Validators.compose([Validators.required , Validators.maxLength(15),Validators.minLength(8)])]
    })

    this.route.params.subscribe(params => {
      this.getUserData.getUserData(params['id']).subscribe((res: any) => {
        this.userData = res;
        this.editUserForm.patchValue({
          fullname : this.userData.name,
          username : this.userData.username,
          email : this.userData.email,
          role : this.userData.role,
          password : this.userData.password,
        })
      })
    })
  }
 
  onEdit(){
    this.route.params.subscribe(params => {
      this.editProfile.editprofile(this.editUserForm.value, params['id']).subscribe((res:any) =>{
        this.toastr.success('Successsfully ',res.message,{
          timeOut: 2500
        });
      })
    })
    this.router.navigate(['/dashboard/user']);
  }

}
