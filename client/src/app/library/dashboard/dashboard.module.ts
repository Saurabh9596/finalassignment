import { NgModule, Compiler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageFilesComponent } from './components/manage-files/manage-files.component';
import { AddProductComponent } from './components/manage-Product/add-product/add-product.component';
import { ListProductComponent } from './components/manage-Product/list-product/list-product.component';
import { UpdateProductDataComponent } from './components/manage-Product/update-product-data/update-product-data.component';
import { DashboardDisplayComponent } from './components/dashboard-display/dashboard-display.component';
import { DashboardRoutingModule } from './/dashboard-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductOperationComponent } from './components/manage-Product/product-operation/product-operation.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { ManageUsersComponent } from './components/user-management/manage-users/manage-users.component';
import { DashboardComponent } from './dashboard.component';
import { EditUserComponent } from './components/user-management/edit-user/edit-user.component';
import { AddUserComponent } from './components/user-management/add-user/add-user.component';
import { AddblogComponent } from './components/blog_components/addblog/addblog.component';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import { ListBlogComponent } from './components/blog_components/list-blog/list-blog.component';
import { UserListblogComponent } from './components/user/user-listblog/user-listblog.component';
import { UserHomeComponent } from './components/user/user-home/user-home.component';
import {RatingModule} from 'primeng/rating';
import { UserProductDisplayComponent } from './components/user/user-product-display/user-product-display.component';
import {CardModule} from 'primeng/card';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {DialogModule} from 'primeng/dialog';
import { UserProductCartComponent } from './components/user/user-product-cart/user-product-cart.component';
import {CarouselModule} from 'primeng/carousel';
import {SpinnerModule} from 'primeng/spinner';
import {DropdownModule} from 'primeng/dropdown';
import { JitCompiler } from '@angular/compiler';
import { EditprofileComponent } from './components/editprofile/editprofile.component';
import { EditBlogComponent } from './components/blog_components/edit-blog/edit-blog.component';
import { OrderSummaryComponent } from './components/user/order-summary/order-summary.component';
import { EditProfileAdminComponent } from './components/edit-profile-admin/edit-profile-admin.component';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    TooltipModule,
    InputTextModule,
    ReactiveFormsModule,
    HttpClientModule,
    CalendarModule,
    RatingModule,
    TableModule,
    PaginatorModule,
    CardModule,
    ScrollPanelModule,
    DialogModule,
    CarouselModule,
    SpinnerModule,
    DropdownModule,
    FormsModule,
  ],
  declarations: [ManageFilesComponent,DashboardDisplayComponent, AddProductComponent, ListProductComponent, UpdateProductDataComponent , ProductOperationComponent, ManageUsersComponent, DashboardComponent, EditUserComponent, AddUserComponent, AddblogComponent,UserHomeComponent, ListBlogComponent, UserListblogComponent, UserProductDisplayComponent, UserProductCartComponent, EditprofileComponent, EditBlogComponent, OrderSummaryComponent, EditProfileAdminComponent] , //, DashboardAdminComponent , BlogComponent, , UserHomeComponent 
  providers: [ 
    {
        provide: Compiler, useExisting: JitCompiler,
    },
]
})
export class DashboardModule { }
