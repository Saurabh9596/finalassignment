import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetUserDataService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  getUserData(id):Observable<any>{
    //'http://localhost:3011/getUserDetails?id='
    return this.http.get(this.baseUrl+'getUserDetails?id='+id);
  }
}
