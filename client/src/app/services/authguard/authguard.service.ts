import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate{
  canActivate(){
    if(localStorage.getItem("Token :") !=null){
    return true 
    }
    else{
      alert('Authentication Fail');
      return false
    }
  }

  constructor() { }
}
