import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EditCartService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  editCart(data: any) : Observable<any>{
    // http://localhost:3011/cart
    return this.http.put(this.baseUrl+'cart',data);
  }
}
