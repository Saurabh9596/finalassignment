import { TestBed } from '@angular/core/testing';

import { EditCartService } from './edit-cart.service';

describe('EditCartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditCartService = TestBed.get(EditCartService);
    expect(service).toBeTruthy();
  });
});
