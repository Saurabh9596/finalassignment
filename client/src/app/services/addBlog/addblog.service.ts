import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddblogService {
  baseUrl = environment.baseurl;
  constructor(private http:HttpClient) { }
  addBlog(data : any) : Observable<any>{
    console.log("Product Data:",data);
    // return http://localhost:3011/blog
    return this.http.post(this.baseUrl+'blog',data);
  }
}
