import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UpdateProductService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  updateData(data : any,id) : Observable<any>{
    // return  'http://localhost:3011/product?id='
    return this.http.put(this.baseUrl+'product?id='+id,data);
  }
}
