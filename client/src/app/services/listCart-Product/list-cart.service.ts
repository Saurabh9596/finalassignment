import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListCartService {
  baseUrl = environment.baseurl;

  constructor(private http: HttpClient) { }
  cartlist(userId) : Observable <any>{
    //'http://localhost:3011/cart?userId='
    return this.http.get(this.baseUrl+'cart?userId='+userId);
  }
}
