import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EdituserService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  editUser(data : any,id) : Observable<any>{
    // return http://localhost:3011/user?id=
    return this.http.put(this.baseUrl+'user?id='+id,data);
  }
}
