import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  baseUrl = environment.baseurl;

  constructor(private http: HttpClient) { }
  makePayment(data: any): Observable<any> {
    //  'http://localhost:3011/Payment'
    return this.http.post(this.baseUrl+'Payment', data);
  }
}