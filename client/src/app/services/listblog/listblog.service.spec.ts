import { TestBed } from '@angular/core/testing';

import { ListblogService } from './listblog.service';

describe('ListblogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListblogService = TestBed.get(ListblogService);
    expect(service).toBeTruthy();
  });
});
