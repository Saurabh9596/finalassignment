import { TestBed } from '@angular/core/testing';

import { RemoveItemFromCartService } from './remove-item-from-cart.service';

describe('RemoveItemFromCartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemoveItemFromCartService = TestBed.get(RemoveItemFromCartService);
    expect(service).toBeTruthy();
  });
});
