import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RemoveItemFromCartService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  deleteItem(id):Observable<any>{
    //http://localhost:3011/cart?id=
    return this.http.delete(this.baseUrl+'cart?id='+id);
  }
}
