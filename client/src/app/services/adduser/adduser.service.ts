import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdduserService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  addUser(data : any) : Observable<any>{
    // return 'http://localhost:3011/adduser'
    return this.http.post(this.baseUrl+'adduser',data);
  }
}
