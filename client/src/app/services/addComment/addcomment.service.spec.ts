import { TestBed } from '@angular/core/testing';

import { AddcommentService } from './addcomment.service';

describe('AddcommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddcommentService = TestBed.get(AddcommentService);
    expect(service).toBeTruthy();
  });
});
