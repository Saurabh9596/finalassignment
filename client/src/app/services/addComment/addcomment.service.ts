import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AddcommentService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  addComment(data:any) : Observable<any>{
    console.log("Comment & Rating:",data);
    // return http://localhost:3011/comment
    return this.http.post(this.baseUrl+'comment',data);
  }
}
