import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteCartService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  deleteCart(id):Observable<any>{
    //'http://localhost:3011/deletecart?id='
    return this.http.delete(this.baseUrl+'deletecart?id='+id);
  }
}

