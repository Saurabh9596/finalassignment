import { TestBed } from '@angular/core/testing';

import { AddToOrdersService } from './add-to-orders.service';

describe('AddToOrdersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddToOrdersService = TestBed.get(AddToOrdersService);
    expect(service).toBeTruthy();
  });
});
