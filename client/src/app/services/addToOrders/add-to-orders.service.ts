import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddToOrdersService {
  baseUrl = environment.baseurl;

  constructor(private http: HttpClient) { }
  addToOrders(data: any): Observable<any> {
    //'http://localhost:3011/Orders'
    return this.http.post(this.baseUrl+'Orders', data);
  }
}
