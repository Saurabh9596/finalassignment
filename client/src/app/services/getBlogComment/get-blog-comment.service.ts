import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetBlogCommentService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  getComments(id): Observable<any> {
    //'http://localhost:3011/comment?id='
    return this.http.get(this.baseUrl+'comment?id='+id);
  }
}
