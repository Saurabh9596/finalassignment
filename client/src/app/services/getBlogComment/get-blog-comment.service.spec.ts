import { TestBed } from '@angular/core/testing';

import { GetBlogCommentService } from './get-blog-comment.service';

describe('GetBlogCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetBlogCommentService = TestBed.get(GetBlogCommentService);
    expect(service).toBeTruthy();
  });
});
