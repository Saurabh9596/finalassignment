import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  forgotPassword(data: any) : Observable<any>{
    // http://localhost:3011/forgotPassword
    return this.http.post(this.baseUrl+'forgotPassword',data);
  }

  resetPassword(data: any , token) : Observable<any>{
    // 'http://localhost:3011/forgotPassword?token='
    return this.http.put(this.baseUrl+'forgotPassword?token='+token,data);
  }
}


