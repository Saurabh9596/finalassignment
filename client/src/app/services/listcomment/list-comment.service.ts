import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListCommentService {
  baseUrl = environment.baseurl;

  constructor(private http: HttpClient) { }
  commentlist(data : any,blogId) : Observable <any>{
    // 'http://localhost:3011/comment?blogId='
    return this.http.put(this.baseUrl+'comment?blogId='+blogId,data);
  }
}
