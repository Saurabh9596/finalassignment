import { TestBed } from '@angular/core/testing';

import { ListCommentService } from './list-comment.service';

describe('ListCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListCommentService = TestBed.get(ListCommentService);
    expect(service).toBeTruthy();
  });
});
