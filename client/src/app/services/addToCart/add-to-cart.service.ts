import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  addtocart(data: any) : Observable<any>{
//http://localhost:3011/cart
    return this.http.post(this.baseUrl+'cart',data);
  }
}
