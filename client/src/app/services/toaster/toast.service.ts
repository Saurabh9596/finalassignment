import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor( private messageService: MessageService) { }

  showSuccess(){
    this.messageService.add({severity:'success', summary: 'Success !', detail:'Login Successfully!'});
  }
  showWarning(){
    this.messageService.add({severity:'warn', summary: 'Warning !', detail:'User is already been Login!'});
  }
  showError(){
    this.messageService.add({severity:'error', summary: 'Error !', detail:'Invalid Credentials!'});
  }
}
