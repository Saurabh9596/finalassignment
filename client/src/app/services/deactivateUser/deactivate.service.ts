import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeactivateService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  deactivateUser(id) : Observable<any>{
    // return http://localhost:3011/deactivate?id=
    return this.http.delete(this.baseUrl+'deactivate?id='+id);
  }
}
