import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderSummaryService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  getOrderSummary(id):Observable<any>{
    // 'http://localhost:3011/Orders?id='
    return this.http.get(this.baseUrl+'Orders?id='+id);
  }
}

