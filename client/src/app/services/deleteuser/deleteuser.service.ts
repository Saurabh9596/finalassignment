import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteuserService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  deleteUser(id):Observable<any>{
    //'http://localhost:3011/user?id='
    return this.http.delete(this.baseUrl+'user?id='+id);
  }
}
