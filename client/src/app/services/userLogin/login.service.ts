import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  checklogin(data : any): Observable<any>{
    // http://localhost:3011/login
    return this.http.post(this.baseUrl+'login',data);
  }
}
