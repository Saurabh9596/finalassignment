import { TestBed } from '@angular/core/testing';

import { EditblogService } from './editblog.service';

describe('EditblogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditblogService = TestBed.get(EditblogService);
    expect(service).toBeTruthy();
  });
});
