import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EditblogService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  EditBlog(data : any,id) : Observable<any>{
    //'http://localhost:3011/blog?id='
    return this.http.put(this.baseUrl+'blog?id='+id,data);
  }

  getBlogData(id):Observable<any>{
    //'http://localhost:3011/getBlogDetails?id='
    return this.http.get(this.baseUrl+'getBlogDetails?id='+id);
  }
}
