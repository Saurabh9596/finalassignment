import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{
  loginToken:any ;
  constructor() { }
 
 
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.loginToken= localStorage.getItem("Token :");
    console.log("login token" ,this.loginToken);
  const authRequest = req.clone({
    headers: req.headers.set("authorization", "Bearer " + this.loginToken )
  });

  // request.clone({
  //   setHeaders: {
  //   Authorization: `Bearer ${localStorage.getItem('Token :')}`
  //   }
  // });

  console.log("Auth Request ",authRequest);
  return next.handle(authRequest);
}
}