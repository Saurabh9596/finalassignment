import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EditprofileService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  editprofile(data : any,id) : Observable<any>{
    // http://localhost:3011/editprofile?id=
    return this.http.put(this.baseUrl+'editprofile?id='+id,data);
  }
}