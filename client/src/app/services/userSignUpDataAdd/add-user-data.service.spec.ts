import { TestBed } from '@angular/core/testing';

import { AddUserDataService } from './add-user-data.service';

describe('AddUserDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddUserDataService = TestBed.get(AddUserDataService);
    expect(service).toBeTruthy();
  });
});
