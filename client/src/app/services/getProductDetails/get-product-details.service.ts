import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetProductDetailsService {
  baseUrl = environment.baseurl;

  constructor(private http : HttpClient) { }
  getProductData(id):Observable<any>{
    // 'http://localhost:3011/getProductDetails?id='
    return this.http.get(this.baseUrl+'getProductDetails?id='+id);
  }
}
