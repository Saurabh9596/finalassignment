import { TestBed } from '@angular/core/testing';

import { GetProductDetailsService } from './get-product-details.service';

describe('GetProductDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetProductDetailsService = TestBed.get(GetProductDetailsService);
    expect(service).toBeTruthy();
  });
});
