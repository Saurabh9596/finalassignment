import { TestBed } from '@angular/core/testing';

import { DeleteblogService } from './deleteblog.service';

describe('DeleteblogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteblogService = TestBed.get(DeleteblogService);
    expect(service).toBeTruthy();
  });
});
