import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteblogService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  deleteBlog(id):Observable<any>{
    //http://localhost:3011/blog/?id=
    return this.http.delete(this.baseUrl+'blog/?id='+id);
  }
}
