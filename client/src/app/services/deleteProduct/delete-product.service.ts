import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteProductService {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient) { }
  deleteItem(id):Observable<any>{
    //'http://localhost:3011/product/?id='
    return this.http.delete(this.baseUrl+'product/?id='+id);
  }
}
