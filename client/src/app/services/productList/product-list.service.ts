import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductListService  {
  baseUrl = environment.baseurl;

  constructor(private http:HttpClient){ }
  productlist() : Observable <any>{
    // 'http://localhost:3011/product'
    return this.http.get(this.baseUrl+'product');
  }
}
