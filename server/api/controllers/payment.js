const stripe = require('stripe')('sk_test_6YFLsOwDu4CoM72rpP8LWqA2');
const constFile = require('../../lib/constant')

module.exports = {
    payForOrders: payForOrders,
}

function payForOrders(req, res) {
    const token = req.body.stripeToken;
    const amount = req.body.amount;
    const userId = req.body.userId;

    const charge = stripe.charges.create({
        amount: amount,
        currency: 'usd',
        description: `UserId: ${userId}`,
        source: token,
    });
    data = {
        message: constFile.messages.paymentDone,
        code : 200 ,
        charge: charge
    }
    res.json(data);
}