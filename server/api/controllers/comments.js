var fs = require('fs');
var comment = require('../schema/commentSchema');
var blog = require('../schema/blogSchema');
var user = require('../schema/userSchema');
const constFile = require('../../lib/constant')

module.exports = {
  //Comments 
  addComment: addComment,
  listcomment: listcomment,
  getBlogComments : getBlogComments

}


//------------------------Comments Adding -----------------------------------
function addComment(req, res) {
  let username;
  var record = new comment();
  record.comment = req.swagger.params.comment.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
  let getUserId = req.swagger.params.userId.value;
  let getBlogId = req.swagger.params.blogId.value;
  let getName = req.swagger.params.userName.value;
  record.rating = req.swagger.params.rating.value;
 
  record.userId = getUserId;
  record.blogId = getBlogId;
  let commentArray = [{
    comment: record.comment,
    rating: record.rating,
    userName: getName,
  }];
  blog.findByIdAndUpdate({ _id: getBlogId }, { $push: { comments: commentArray } }, function (err, data) {
    console.log("After getting user data====", data);
    data.save()
      .then(item => {
        console.log("Comments Printing ************", data);
        res.json(record);
      })
      .catch(err => {
        console.log(err);
        res.status(400).send("Unable to save to database");
      });
  });
}

function listcomment(req, res) {
  console.log("Inside ListComment API ", req.swagger.params.blogId.value);
  let id = req.swagger.params.blogId.value;
  // console.log('blog id',id);
  blog.findById(id ,function (err, data) {
    console.log("After Listing blog comment data", data.comments);
    // resData = data;
    res.json(data);
  });
}


function getBlogComments(req ,res){
  var id  = req.swagger.params.id.value ;
  console.log("Blog Id after backend api",id);
  blog.findById(id ,function (err, data) {
    console.log("After Listing blog comment data", data.comments);
    // resData = data;
    res.json(data.comments);
  });
}

