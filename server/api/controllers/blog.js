var fs = require('fs');
var blog = require('../schema/blogSchema');
const constFile = require('../../lib/constant')

module.exports = {
  addblog: addblog,
  listblog: listblog,
  deleteblog: deleteblog,
  editblog: editblog,
  getBlogDetails: getBlogDetails,
  getBlogComments: getBlogComments

  //Comments 
  // addComment : addComment,
  // listcomment:listcomment
}


function addblog(req, res) {
  //------Image Upload Code----------
  var timeStamp = Date.now();
  var imageName = timeStamp + '_' + req.files.imageUrl[0].originalname;
  console.log("Image name", imageName);
  var path = constFile.imageStoringPathProduct + imageName;
  var retrivingPath = imageName;
  fs.writeFile(path, (req.files.imageUrl[0].buffer), function (err) {
    if (err) throw err;
    console.log('Image Uploaded Successfully ');
  });
  //Static Image path
  // let staticImgPath = 'http://localhost:3011/ImageUploads/blog/';
  var record = new blog();
  record.title = req.swagger.params.title.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
  record.description = req.swagger.params.description.value;
  record.date = req.swagger.params.date.value;
  record.imageUrl = constFile.imageUploadProduct + retrivingPath;
  record.save()
    .then(item => {
      console.log("Blog Printing", record);
      res.json({
        code: 200,
        message: constFile.messages.blogAdded ,//"Blog Added",
        data: record
      });
      // res.json(record);
    })
    .catch(err => {
      console.log(err);
      res.status(400).send(constFile.messages.unableToSave);
    });
  // });
}

function listblog(req, res) {
  console.log("Inside ListData API ");

  var blogDataarray = [{
    $project: {
      title: '$title',
      description: '$description',
      imageUrl: '$imageUrl',
      date: '$date',
      rating: { $avg: '$comments.rating' },
      comments: '$comments'
    }
  }];
  blog.aggregate(blogDataarray, function (err, data) {
    res.json(data);
  })
}

function deleteblog(req, res) {
  var _id = req.swagger.params.id.value;
  console.log("Id to delete", _id);
  blog.findByIdAndRemove(_id, function (err, data) {
    if (err) throw err;
    console.log("Product Deleted Sucessfully!", data);
    data.save();
    res.json({
      code: 200,
      message: constFile.messages.blogDeleted,//"Blog Deleted",
      data: data
    });
  });
}



function editblog(req, res) {

  if (!req.files.imageUrl) {

    let _id = req.swagger.params.id.value
    let title = req.swagger.params.title.value;
    let description = req.swagger.params.description.value;
    blog.findByIdAndUpdate(_id,
      {
        $set: {
          title: title,
          description: description,
        }
      }, function (err, data) {
        if (err) {
          console.log("errr", err)
        } else {
          console.log("data", data)
        }
        data.save();
        res.json({
          code: 200,
          message: constFile.messages.blogUpdate,//"Blog Updated",
          data: data
        });
      });
  }
  else {

    let timeStamp = Date.now();
    let imageName = timeStamp + '_' + req.files.imageUrl[0].originalname;
    console.log("Image name", imageName);
    let path = constFile.imageStoringPathProduct + imageName;
    let retrivingPath = imageName
    fs.writeFile(path, (req.files.imageUrl[0].buffer), function (err) {
      if (err) throw err;
      console.log('Image Uploaded Successfully ');
    });

    // let staticImgPath = 'http://localhost:3011/ImageUploads/blog/';

    let _id = req.swagger.params.id.value
    let title = req.swagger.params.title.value;
    let description = req.swagger.params.description.value;
    let imageUrl = constFile.imageUploadProduct + retrivingPath;

    blog.findByIdAndUpdate(_id,
      {
        $set: {
          title: title,
          description: description,
          imageUrl: imageUrl,
        }
      }, function (err, data) {
        if (err) {
          console.log("errr", err)
        } else {
          console.log("data", data)
        }
        data.save();
        res.json({
          code: 200,
          message: constFile.messages.blogUpdate,//"Blog Updated",
          data: data
        });
      });
  }
};



function getBlogDetails(req, res) {
  var _id = req.swagger.params.id.value;
  console.log("Id to update data", _id);
  blog.findById(_id, function (err, data) {
    if (err) throw err;
    console.log("Get Blog Details data", data);
    res.json(data);
  });
}

function getBlogComments(req, res) {
  var id = req.swagger.params.id.value;
  blog.findById(id, function (err, data) {
    console.log("After Listing blog comment data", data.comments);
    // resData = data;
    res.json(data.comments);
  });
}




  //------------------------Comments Adding -----------------------------------
  // function addComment(req, res) {
  //   var record = new blog();
  //   record.comment = req.swagger.params.comment.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
  //   // record.rating = req.swagger.params.rating.value;
  //   record.save()
  //   .then(item => {
  //       console.log("Comments Printing", record);
  //       res.json(record);
  //       // res.json(record);
  //   })
  //   .catch(err => {
  //       console.log(err);
  //       res.status(400).send("Unable to save to database");
  //   });
  // }

  // function listcomment(req, res) {
  //   console.log("Inside ListData API ");
  //   blog.find(function (err, data) {
  //     console.log("After Listing comment", data.comment);
  //     // resData = data;
  //     res.json(data.comment);
  //   });
  // }