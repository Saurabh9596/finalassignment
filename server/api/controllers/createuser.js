
var jwt = require('jsonwebtoken');
var async = require('async');
var nodemailer = require('nodemailer');
// var smtpTransport = require('nodemailer-smtp-transport');

var crypto = require('crypto');
const constFile = require('../../lib/constant')
const userModel = require('../schema/userSchema.js');
var token1 = jwt.sign({ foo: 'bar' }, 'shhhhh');
var product = require('../schema/productSchema');
var cartModel = require("../schema/cartSchema")
var mongoose = require('mongoose');
module.exports = {
    createuser: createuser,
    login: login,
    listuser: listuser,
    deleteuser: deleteuser,
    editUser: editUser,
    addUser: addUser,
    deactivate: deactivate,
    getUserDetails: getUserDetails,

    //------Edit Profile ---
    editProfile: editProfile,

    //-----Forgot Password ----
    forgotPassword: forgotPassword,
    resetPassword: resetPassword,
    finalReset: finalReset,

    //------*** Add To Cart ***------
    addtocart: addtocart,
    listCart: listCart,
    editCart: editCart,
    deleteProductFromCart: deleteProductFromCart,
    deleteCart: deleteCart,
}

function createuser(req, res) {
    let user = new userModel({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        role: req.body.role,
        isDeactivate: 'Active'

    })
    console.log("Role", user.role)
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: constFile.messages.userNotAdded //'user not Added'
            })
        } else {
            res.json({
                code: 200,
                message:  constFile.messages.userAdded ,// "User Added",
                data: response
            })
            console.log('Successfully created');
        }
    })
}

function login(req, res) {
    let data = {
        username: req.body.username,
        password: req.body.password,
    };
    userModel.findOne(data).lean().exec(function (err, user) {
        if (err) {
            return res.json({ error: true });
        }
        if (!user) {
            return res.json({ message: constFile.messages.userNotFound ,//'User not found!', 
            code: 404,
             data: {} });
        }
        console.log(user);
        let token = jwt.sign(user, "Saurabh", {
            expiresIn: 3600000 // 1440 expires in 1 hour
        });
        res.json({ message:  constFile.messages.loginSuccess,//"Login Successfull !", 
        token: token, 
        code: 200, 
        data: user }); //, user : user
    })

}

function listuser(req, res) {
    // var dataArray = [{
    //     $project : {
    //       name : '$name',
    //       email : '$email',
    //       contact : "$contact",
    //       username : "$username",
    //       password : "$password",
    //     }
    //   }];
    console.log("Inside ListUser API ");
    userModel.find(function (err, data) {
        // console.log("After Listing data", data);
        res.json(data);
    });
}

function deleteuser(req, res) {
    var _id = req.swagger.params.id.value;
    console.log("Id to delete", _id);
    userModel.findByIdAndRemove(_id, function (err, data) {
        if (err) throw err;
        console.log("Record Deleted Sucessfully!", data);
        data.save();
        res.json({
            code: 200,
            message:  constFile.messages.userDeleted,//"User Deleted",
            data: data
        });
        // data.save();  
    });
}

function addUser(req, res) {
 
    let user = new userModel({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        role:  'user',
        isDeactivate: 'Active'
    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: constFile.messages.warning,//'user not Added'
            })
        } else {
            res.json({
                code: 200,
                message:  constFile.messages.userRegister,//"User Registered ",
                data: response
            })
            console.log('Successfully created');
        }
    })
 
   
}


function editUser(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    var name = req.body.name;
    var password = req.body.password;
    var email = req.body.email;
    var role = req.body.role;
    console.log(name)
    userModel.findByIdAndUpdate(_id, { $set: { username: name, password: password, email: email, role: role } }, function (err, data) {
        data.save();
        if (err) {
            console.log("errr", err)
            res.json({
                code: 200,
                message:  constFile.messages.err,//"User data Updated",
                data: data
            });
        } else {
            res.json({
                code: 200,
                message: constFile.messages.userDataUpdated,// "User data Updated",
                data: data
            });
        }

    });
};

//-------Edit Profile -----
function editProfile(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    let name = req.body.fullname;
    let username = req.body.username;
    console.log("Username ", name);
    let password = req.body.password;
    let email = req.body.email;
    let role = req.body.role;
    userModel.findByIdAndUpdate(_id, { $set: { name : name , username: username, password: password, email: email, role: role } }, function (err, data) {
        data.save();
        if (err) {
            console.log("errr", err)
        } else {
            res.json({
                code: 200,
                message: constFile.messages.profileUpdated,//"Profile Updated ",
                data: data
            });
        }
    });
};

function deactivate(req, res) {
    var _id = req.swagger.params.id.value;
    console.log("Id to deactivate", _id);
    var active_status;
    userModel.findById(_id, function (err, data) {
        if (err) {
            res.json(err);
        } else if (data) {
            var status = data.isDeactivate;
            active_status = (status == 'Active') ? 'Deactive' : 'Active';
            userModel.findByIdAndUpdate(_id, { $set: { isDeactivate: active_status } }, function (err, data) {
                if (err) {
                    res.json(err);
                } else {
                    data.save();
                    res.json({
                        code: 200,
                        message: `${constFile.messages.userStatus} : ${active_status} `,
                        data: data
                    });
                }
            });
        } else {
            res.json("Final else");
        }
    });
}


function getUserDetails(req, res) {
    var _id = req.swagger.params.id.value;
    console.log("Id to update data", _id);
    userModel.findById(_id, function (err, data) {
        if (err) throw err;
        // data.save();
        // console.log("User Data to set patch Value", data);
        res.json(data);
    });
}



//---------********************** Add to Cart Module ***************-------------------------

function addtocart(req, res) {

    var cartItem = new cartModel();
    cartItem.quantity = req.body.quantity | 1;
    cartItem.date = Date.now();
    cartItem.productId = req.body.productId;
    cartItem.productCost = req.body.productCost;
    cartItem.totalbill = (cartItem.quantity * cartItem.productCost);
    cartItem.userId = req.body.userId;
    console.log(cartItem);


    cartModel.findOne({ 'productId': cartItem.productId, "userId": cartItem.userId }, function (err, data) {
        if (err) throw err;
        else if (data) {
            console.log("same product");
            console.log("Complete Product Data", data);
            data.quantity = Number(cartItem.quantity + data.quantity);
            data.totalbill = (cartItem.productCost * data.quantity);
            cartModel.updateOne({ "productId": data.productId }, { $set: data }, function (err, result) {
                if (err) throw err;
                else {
                    res.json(result);
                }
                console.log("Cart updated");
            });
        }
        else {
            cartItem.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: constFile.messages.cartDetNotAdded ,//'CartDetails not Added'
                    })
                } else {
                    res.json({
                        code: 200,
                        message:  constFile.messages.productAdded ,//"Product Added to Cart",
                        data: response
                    })
                    console.log('Successfully added to cart');

                }
            })

        }
    })

}



function listCart(req, res) {
    cartModel.find({ userId: req.swagger.params.userId.value }).sort({ date: 'descending' }).populate({
        path: 'productId',
        model: 'ProductSchema',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 404,
                message: "Error"
            })
        } else {
            // let total = 0 ;
            // for (let i = 0; i < data.length; i++) {
            //     total = total + data.totalbill;
            // }
            // console.log('Total Bill ', this.totalbill);
            res.json(data);
        }
    })

}


function deleteProductFromCart(req, res) {
    let id = req.swagger.params.id.value;
    console.log("Id to delete", id);
    cartModel.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) throw err;
        console.log("Product Deleted Sucessfully from Cart!", data);
        data.save();
        res.json({ code: 200,
             message: constFile.messages.productDeleted,//"Product Deleted from Cart",
              data: data });
    });
}


function deleteCart(req, res) {
    let id = req.swagger.params.id.value;
    cartModel.deleteMany({ userId: id }, function (err, data) {
        if (err) throw err;
        console.log(" Deleted Record Sucessfully from Cart!", data);
        res.json(data);
    });

}


function editCart(req, res) {

    var cartRecord = new cartModel();
    cartRecord.date = Date.now();
    cartRecord.userId = req.body.userId;
    cartRecord.productId = req.body.productId;
    cartRecord.productCost = req.body.productCost;
    cartRecord.quantity = (req.body.quantity);
    cartRecord.totalbill = (cartRecord.quantity * cartRecord.productCost);
    console.log(cartRecord);

    cartModel.findOne({ "userId": cartRecord.userId, 'productId': cartRecord.productId }, function (err, data) {
        if (err) throw err;
        else if (data) {
            console.log("same product");
            console.log("Complete Product Data", data);
            data.quantity = Number(cartRecord.quantity);
            data.totalbill = (cartRecord.productCost * data.quantity);
            cartModel.updateOne({ "productId": data.productId }, { $set: data }, function (err, result) {
                if (err) throw err;
                else {
                    res.json(result);
                }
                console.log("Cart updated");
            });
        }
        else {
            cartRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: constFile.messages.cartDetNotAdded,//'CartDetails not Added'
                    })
                } else {
                    res.json({
                        code: 200,
                        message: constFile.messages.cartUpdated,
                        data: response
                    })
                    console.log('Successfully added to cart');

                }
            })

        }
    })

}




//---***********Forgot Password ****------------------
function forgotPassword(req, res) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            userModel.findOne({ email: req.body.email }, function (err, user) {
                if (!user) {
                    console.log(constFile.messages.err, constFile.messages.noAccountWithEmail);
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user, done) {
            var smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                host: 'smtp.gmail.com',
                secure: true,
                port: 4,
                requireTLS: true,
                auth: {
                    user: 'smartianSaurabh@gmail.com',
                    pass: 'smartianSaurabh9596'
                },
            }
            );
            // + req.headers.host +
            var mailOptions = {
                to: user.email,
                from: 'smartianSaurabh@gmail.com',
                subject: constFile.messages.userAccountPasswordReset ,//'User Account Password Reset',
                text: constFile.messages.textToSentOnEmailPasswordReset1  + token + '\n\n' +
                constFile.messages.textToSentOnEmailPasswordReset2
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                console.log('An e-mail has been sent to ' + user.email + ' with further instructions.');
                done(err, token, 'done');
            });
        }
    ], function (err, token) {
        if (err) return next(err);
        console.log("token :", token);
        res.json({
            code: 200,
            message: constFile.messages.requestForResetPassword,//"Request for Reset Password Done",
            token: token
        })
    });
}

//request contains token generated after forgot password link 
function resetPassword(req, res) {
    userModel.findOne({ resetPasswordToken: req.swagger.params.id.value, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
        if (!user) {
            console.log('Password reset token is invalid or has expired.');
        }
        console.log("data of user ", user);
        let data = {
            data: user,
            message: constFile.messages.requestConfirmed,//"Reset Request Confirmed!"
        }
        res.json(data);
    });
}


//final reset password 
function finalReset(req, res) {
    async.waterfall([
        function (done) {
            console.log(req.swagger.params.token.value);
            userModel.findOne({ resetPasswordToken: req.swagger.params.token.value, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
                if (!user) {
                    console.log('Password reset token is invalid or has expired.');
                    return res.json({ code: 404, 
                        message: constFile.messages.resetTokenInvalid,//'Password reset token is invalid or has expired.',
                         data: {} });
                }

                user.password = req.body.password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;
                console.log("Updated User Data", user);
                user.save(function (err) {
                    done(err, user);
                });
            });
        },
        function (user, done) {
            var smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                host: 'smtp.gmail.com',
                secure: true,
                port: 4,
                requireTLS: true,
                auth: {
                    user: 'smartianSaurabh@gmail.com',
                    pass: 'smartianSaurabh9596'
                },
            });
            // var smtpTransport = nodemailer.createTransport(transOptions);
            var mailOptions = {
                to: user.email,
                from: 'smartianSaurabh@gmail.com',
                subject: constFile.messages.passwordHasChanged,
                text: constFile.messages.confirmationText1 + user.email + constFile.messages.confirmationText2
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                console.log('Success! Your password has been changed.');
                done(err);
            });
        }
    ], function (err) {
        if (err) return next(err);
        console.log(err)
        res.json({
            code: 200,
            message:  constFile.messages.resetPasswordDone, //"Reset Password Done ",
            data: {}
        })
    });

    // console.log("Token :" , req.swagger.params.token.value);
    // console.log("Token :" , req.body.newpassword);
    // async.waterfall([
    //     function (done) {
    //         userModel.findOne({ resetPasswordToken: req.body.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
    //             console.log("user : ", user)
    //             if (!user) {
    //                 return res.json({msg: "User not found"});
    //                 console.log('Password reset token is invalid or has expired.');
    //             }

    //             user.password = req.body.newpassword;
    //             console.log("New Password" , user.password);
    //             user.resetPasswordToken = undefined;
    //             user.resetPasswordExpires = undefined;

    //             user.save(function (err) {
    //                 done(err, user.resetPasswordToken, user);
    //             });
    //         });
    //     },
    //     function (user, done) {
    //         var smtpTransport = nodemailer.createTransport({
    //             service: 'Gmail',
    //             host: 'smtp.gmail.com',
    //             secure: true,
    //             port: 4,
    //             requireTLS: true,
    //             auth: {
    //                 user: 'smartianSaurabh@gmail.com',
    //                 pass: 'smartianSaurabh9596'
    //             },
    //         });
    //         var mailOptions = {
    //             to: user.email,
    //             from: 'smartianSaurabh@gmail.com',
    //             subject: 'Your password has been changed',
    //             text: 'Hello,\n\n' +
    //                 'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
    //         };
    //         smtpTransport.sendMail(mailOptions, function (err) {
    //             console.log('Success! Your password has been changed.');
    //             done(err);
    //         });
    //     }
    // ], function (err, data) {
    //     //   res.redirect('/');
    //     if (err) {
    //         console.log("Error : ", err);
    //     }
    //     else {
    //         res.json("Final Response", data);
    //     }
    // });
}




//------***********************Single Table AddToCart Query  *******************----------------------


 // let userId = mongoose.Types.ObjectId(req.body.userId);
    // let productId = mongoose.Types.ObjectId(req.body.productId);
    // let quantity = req.body.quantity;
    // console.log("Cart Data:", typeof (userId), productId);
    // userModel.aggregate([
    //     { $unwind: "$cart" },
    //     { $match: { "cart.productId": productId , "_id":userId} },
    //     { $project: { "_id": 0, "productId": "$cart.productId", "quantity": "$cart.quantity" } }
    // ], function (err, data) {

    //     if (data[0] == null) {
    //         let newCart = [{
    //             productId: productId,
    //             quantity: quantity,
    //             totalbill: 0
    //         }]
    //         console.log(newCart[0])
    //         userModel.findByIdAndUpdate(userId, {
    //             $push: {
    //                 cart: newCart
    //             }
    //         }, function (err, data) {
    //             // console.log("After pushing Data",data);
    //             data.save();
    //             if (err) {
    //                 console.log("errr", err)
    //             } else {
    //                 console.log("data", data)
    //             }
    //             res.json(data);
    //         });
    //         return data;
    //     }
    //     else {
    //         let qty = quantity + (data[0].quantity);

    //         userModel.findOneAndUpdate(
    //             { "_id": userId, "cart.productId": productId },
    //             {
    //                 "$set": {
    //                     "cart.$.quantity": qty
    //                 }
    //             },
    //             function (err, data) {
    //                 if (err) {
    //                     console.log("error-------------------", err)
    //                 } else {
    //                     console.log("data--------------------", data)
    //                 }
    //                 res.json(data);
    //             });
    //     }
    // });

