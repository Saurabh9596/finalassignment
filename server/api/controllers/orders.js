 //------*** Add To Orders ***------
const userModel = require('../schema/userSchema.js');
const cartModel = require("../schema/cartSchema");
const orders = require("../schema/orderSchema");
const mongoose = require('mongoose');
const constFile = require('../../lib/constant')


module.exports = {
    //------*** Add To Cart ***------
    addToOrders: addToOrders,
    listorder : listorder
}

function addToOrders(req, res) {
    var orderRecord = new orders();

    orderRecord.date = Date.now();
    orderRecord.userId = req.body.userId;
    orderRecord.paymentMethod = req.body.paymentMethod;
    orderRecord.address = req.body.address;
    orderRecord.total = req.body.total;
    cartModel.find({ userId: orderRecord.userId }).sort({ date: 'descending' }).populate({
        path: 'productId',
        model: 'ProductSchema',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: constFile.messages.pleaseTryAgain
            })
        } else {
            orderRecord.cartData = data;
            console.log("*****************************", orderRecord.cartData);
            orderRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: constFile.messages.orderNotAdded
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response
                    })
                    console.log('Successfully added to Orders');

                }
            })
        }
    })
    // console.log("OrderRecord:", orderRecord);

}

function listorder(req, res) {
    orders.find({ userId: req.swagger.params.id.value }).sort({ date: 'descending' }).populate({
        path: 'cartData.productId',
        model: 'ProductSchema',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: constFile.messages.pleaseTryAgain
            })
        } else {
          
            res.json(data);
        }
    })
    
  }

