const userModel = require('../schema/userSchema.js');
var fs = require('fs');
var product = require('../schema/productSchema');
const constFile = require('../../lib/constant')

module.exports = {
  addproduct: addproduct,
  listproduct: listproduct,
  deleteproduct: deleteproduct,
  editproduct: editproduct,
  getProductDetails: getProductDetails
}

function addproduct(req, res) {
  //------Image Upload Code----------
  var timeStamp = Date.now();
  var imageName = timeStamp + '_' + req.files.imageUrl[0].originalname;

  var path = constFile.imageStoringPathProduct + imageName;
  var retrivingPath = imageName;
  fs.writeFile(path, (req.files.imageUrl[0].buffer), function (err) {
    if (err) throw err;
    console.log('Image Uploaded Successfully ');
  });

  //Uploads Image Path
  // let staticImgPath = 'http://localhost:3011/ImageUploads/product/';
  var record = new product();
  record.productName = req.swagger.params.productName.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
  record.productCost = req.swagger.params.productCost.value;
  record.description = req.swagger.params.description.value;
  record.delStatus = req.swagger.params.delStatus.value;
  record.shippingCost = req.swagger.params.shippingCost.value;
  record.imageUrl = constFile.imageUploadProduct + retrivingPath;

  record.save()
    .then(item => {
      console.log("Record Printing", record);
      res.json({
        code: 200,
        message: constFile.messages.productAdded,//"Product Added",
        data: record
      });
    })
    .catch(err => {
      console.log(err);
      res.status(400).send(constFile.messages.unableToSave);
    });
  // });
}

function listproduct(req, res) {
  // var dataArray = [{
  //     $project : {
  //       productName : '$productName',
  //       consumerName :'$consumerName',
  //       productCost : '$productCost',
  //       address : "$address",
  //       phNumber : "$phNumber",
  //       delStatus : "$delStatus",
  //       yearMonthDayUTC : {$dateToString : {format : "%Y-%m-%d" , date :"$date"}}
  //     }
  //   }];
  console.log("Inside ListData API ");
  product.find(function (err, data) {
    console.log("After Listing data", data);
    res.json(data);
  });


}

function deleteproduct(req, res) {
  var _id = req.swagger.params.id.value;
  console.log("Id to delete", _id);
  product.findByIdAndRemove(_id, function (err, data) {
    if (err) throw err;
    console.log("Product Deleted Sucessfully!", data);
    data.save();
    res.json({
      code: 200,
      message: constFile.messages.productDeleted ,//"Product Deleted ",
      data: data
    });
  });
}

function editproduct(req, res) {

  if (!req.files.imageUrl) {

    let _id = req.swagger.params.id.value
    console.log(_id);
    let productname = req.swagger.params.productName.value;
    let productcost = req.swagger.params.productCost.value;
    console.log("product name", productcost);
    let description = req.swagger.params.description.value;
    let delStatus = req.swagger.params.delStatus.value;
    let shippingCost = req.swagger.params.shippingCost.value;
    product.findByIdAndUpdate(_id,
      {
        $set: {
          productName: productname,
          productCost: productcost,
          description: description,
          delStatus: delStatus,
          shippingCost: shippingCost,
        }
      }, function (err, data) {
        if (err) {
          console.log("errr", err)
        } else {
          console.log("data", data)
        }
        data.save();
        res.json({
          code: 200,
          message: constFile.messages.updateRecord, //"Updated Record ",
          data: data
        });
      });
  }
  else {

    let timeStamp = Date.now();
    let imageName = timeStamp + '_' + req.files.imageUrl[0].originalname;
    console.log("Image name", imageName);
    let path = constFile.imageStoringPathProduct + imageName;
    let retrivingPath = imageName
    fs.writeFile(path, (req.files.imageUrl[0].buffer), function (err) {
      if (err) throw err;
      console.log('Image Uploaded Successfully ');
    });


    // let staticImgPath = 'http://localhost:3011/ImageUploads/product/';

    let _id = req.swagger.params.id.value
    console.log(_id);
    let productname = req.swagger.params.productName.value;
    let productcost = req.swagger.params.productCost.value;
    console.log("product name", productcost);
    let description = req.swagger.params.description.value;
    let delStatus = req.swagger.params.delStatus.value;
    let shippingCost = req.swagger.params.shippingCost.value;
    let imageUrl = constFile.imageUploadProduct + retrivingPath;
    product.findByIdAndUpdate(_id,
      {
        $set: {
          productName: productname,
          productCost: productcost,
          description: description,
          delStatus: delStatus,
          shippingCost: shippingCost,
          imageUrl: imageUrl
        }
      }, function (err, data) {
        if (err) {
          console.log("errr", err)
        } else {
          console.log("data", data)
        }
        data.save();
        res.json({
          code: 200,
          message: constFile.messages.updateRecord,//"Updated Record ",
          data: data
        });
      });

  }

};


function getProductDetails(req, res) {
  var _id = req.swagger.params.id.value;
  console.log("Id to update data", _id);
  product.findById(_id, function (err, data) {
    if (err) throw err;
    // data.save();
    console.log("Get Product Details data", data);
    res.json(data);
  });
}