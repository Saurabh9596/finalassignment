const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
let CartSchema = new mongoose.Schema({
    userId: { type: String, ref:'UserSchema' },
    productId : {type : String , ref:'ProductSchema' },
    productCost: {type: Number},
    date: { type: Date , default : Date.now()},
    totalbill : {type : Number} ,
    // orderstatus : {type : String},
    quantity : {type : Number} ,
});
const cartModel = mongoose.model('CartSchema', CartSchema);
module.exports = cartModel;




