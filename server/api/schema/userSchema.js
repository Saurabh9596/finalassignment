const mongoose = require('mongoose');
let UserSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String , unique : true },
    username: { type: String , unique : true },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    contact: { type: Number },
    password: { type: String },
    role : {type : String},
    isDeactivate : {type : String},
    token : {type : String},
    cart : {type : Array} ,  
});
const userModel = mongoose.model('UserSchema', UserSchema);
module.exports = userModel;

