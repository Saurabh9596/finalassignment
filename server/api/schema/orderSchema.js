
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var  ordersSchema = new mongoose.Schema({
    date: {type: Date, default: Date.now()},
    userId:{type: Schema.Types.ObjectId,ref:"UserSchema"},
    cartData: Array,
    paymentMethod: String,
    address : String,
    total : Number,
});

var ordersSchema = mongoose.model("OrdersSchema", ordersSchema);
module.exports =ordersSchema;

