var mongoose = require('mongoose');

var productSchema = new mongoose.Schema({
    productName: String,
    productCost: Number,
    description : String ,
    shippingCost: Number,
    delStatus : String,
    imageUrl: String,
    rating : Number

    // consumerName: String,
    // address : String,
    // phNumber : Number,
    // date : String
});

module.exports = mongoose.model('ProductSchema', productSchema);

