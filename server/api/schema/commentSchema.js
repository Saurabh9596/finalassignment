var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    rating:Number,
    comment : String,
    // reply : Array,
    // blogId : String,
    userId : String ,
    userName : String
});

module.exports = mongoose.model('commentSchema', commentSchema);

