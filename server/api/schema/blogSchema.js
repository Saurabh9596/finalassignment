var mongoose = require('mongoose');

var blogSchema = new mongoose.Schema({
    title: String,
    description: String,
    imageUrl : String ,
    date : Date,
    rating:Number,
    comments : Array
});

module.exports = mongoose.model('blogSchema', blogSchema);

