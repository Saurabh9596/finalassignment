var mongoose = require('mongoose');

var imageSchema = new mongoose.Schema({
    imageUrl: String,
});

module.exports = mongoose.model('ImageSchema', imageSchema);

