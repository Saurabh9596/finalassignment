
const messages = {
    "userNotAdded": "User Not Added",
    "userAdded": "User Added",
    "userRegister": "User Registered ",
    "userNotFound": "User Not Found",
    "userNotFound": "User Not Found",
    "userDeleted": "User Deleted",
    "loginSuccess": "Login Successfull !",
    "userDataUpdated": "User data Updated",
    "profileUpdated": "Profile Updated ",
    "userStatus": "User Status",
    "cartDetNotAdded": "CartDetails not Added",
    "productAdded": "Product Added to Cart",
    "productDeleted": "Product Deleted from Cart",
    "cartUpdated": "Cart Data Updated",
    "warning": "Warning",
    "badRequest": "BadRequest",
    "err": "Error",

    "noAccountWithEmail": "No account with that email address exists.",
    "userAccountPasswordReset": "User Account Password Reset",
    "textToSentOnEmailPasswordReset1": "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n Please click on the following link, or paste this into your browser to complete the process:\n\n http://localhost:4200/auth/resetPassword/",
    "textToSentOnEmailPasswordReset2": "If you did not request this, please ignore this email and your password will remain unchanged.\n",
    "requestForResetPassword": "Request for Reset Password Done",
    "requestConfirmed": "Reset Request Confirmed!",
    "resetTokenInvalid": "Password reset token is invalid or has expired.",
    "passwordHasChanged": "Your password has been changed",
    "confirmationText1": "Hello,\n\n This is a confirmation that the password for your account ",
    "confirmationText2": " has just been changed.\n\n Electrovilla",
    "resetPasswordDone": " Reset Password Done ",

    "productAdded": "Product Added",
    "unableToSave": "Unable to save to database",
    "productDeleted": "Product Deleted ",
    "updateRecord": "Updated Record ",

    "blogAdded": "Blog Added",
    "blogDeleted": "Blog Deleted ",
    "blogUpdate": "Blog Updated",

    "addToOrders": "added to Orders",
    "orderNotAdded": "OrderDetails not Added",
    "pleaseTryAgain": "Please Try Again",

    "paymentDone": "Payment Done",

  
}
const validationMessages = {

    "KeyValueError": "Bad Request ! Key  or Value is missing."
}
const connectionMessages = {
    "connected": "Database connected Successfully.",
    "connectionErr": "Failed to connect to Data base."
}

const statusCode = {
    "success": 200,
    "error": 401,
    "warning": 404,
    "failed": 1002,
    "internalError": 1004,
    "failedConnection": 500,
    "okList": 201
}
const webToken = {
    "privateKey": "secret123"
}
const baseUrl = 'http://localhost:4200';
const imageStoringPathProduct = '../server/public/ImageUploads/product/';
const imageUploadProduct = 'http://localhost:3011/ImageUploads/product/';

const imageStoringPathBlog = '../server/public/ImageUploads/blog/';
const imageUploadBlog = 'http://localhost:3011/ImageUploads/blog/';


const obj = {
    messages: messages,
    validationMessages: validationMessages,
    statusCode: statusCode,
    connectionMessages: connectionMessages,
    webToken: webToken,
    baseUrl: baseUrl,
    imageStoringPathProduct: imageStoringPathProduct,
    imageUploadProduct: imageUploadProduct,

    imageStoringPathBlog: imageStoringPathBlog,
    imageUploadBlog: imageUploadBlog
};
module.exports = obj;