'use strict';


var jwt = require('jsonwebtoken');
const User = require('../api/schema/userSchema');


module.exports = {
    ensureAuthorized: ensureAuthorized
}


function ensureAuthorized(req, res, next) {
    var unauthorizedJson = { code: 401, 'message': 'Unauthorized', data: {} };
    var token = req.headers["authorization"];

    //if (req.headers.authorization) {
    if (typeof token !== 'undefined') {
        //var token = req.headers.authorization;
        var splitToken = token.split(' ');
        try {
            token = splitToken[1];
            var decoded = jwt.verify(token, 'Saurabh');
            req.user = decoded
            if (decoded) {
                if (splitToken[0] == 'Bearer') {
                    User.findOne({ _id: decoded._id }).exec(function (err, user) {
                        if (err || !user) {
                            res.json(unauthorizedJson);
                        } else {
                            req.user = user;
                            next();
                        }
                    });
                } else {
                    res.json(unauthorizedJson);
                }

            } else {
                res.json(unauthorizedJson);
            }

        } catch (err) {
            res.json(unauthorizedJson);
        }
    } else {
        res.json(unauthorizedJson);
    }
}