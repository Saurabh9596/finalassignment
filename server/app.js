'use strict';
var express = require('express');
var path = require('path');
var SwaggerExpress = require('swagger-express-mw');
var app = express();
var mongoose = require('mongoose');
var cors = require('cors');
var utils = require('./lib/util');

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

mongoose.connect('mongodb://localhost:27017/SwaggerCreateUser');
mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'error in connection:'));
db.once('open', function (callback) {
  console.log('Database connected!');
});


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE,OPTIONS');
  next();
});


app.use('/api/*', function (req, res, next) {
  var freeAuthPath = [
    '/api/adduser',
    '/api/login',
    '/api/Payment',
    '/api/forgotPassword',
    '/api/reset',

  ];
  var available = false;
  for (var i = 0; i < freeAuthPath.length; i++) {
    if (freeAuthPath[i] == req.baseUrl) {
      available = true;
      break;
    }
  }
  if (!available) {
    utils.ensureAuthorized(req, res, next);
  } else {
    next();
  }
});



SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }

  app.set('views', path.join(__dirname, './dist/client'));

  app.use(express.static(path.join(__dirname, 'public')));
  // install middleware
  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  app.use(cors());
  swaggerExpress.register(app);

  var port = process.env.PORT || 3011;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/docs/#/');
  }
});
